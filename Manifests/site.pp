node default {
    file {'/home/master/README':
        ensure => file,
        content => 'This is a README',
        owner => 'master',
    }
}
